<?php

use Illuminate\Database\Seeder;
use App\Models\Client;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::create([
            'domain' => 'mundotiro.com.br',
            'limit_per_hour' => 1,
            'transport_config' => json_encode([
                'driver'       => 'smtp',
                'host'         => 'smtp.mailtrap.io',
                'port'         => '2525',
                'from_name'    => 'Mundo Tiro',
                'from_address' => 'contato@mundotiro.com.br',
                'encryption'   => 'tls',
                'username'     => 'ce1319547adac4',
                'password'     => 'aec6920f69349c',
            ]),
        ]);

        Client::create([
            'domain' => 'shootinghouse.com.br',
            'limit_per_hour' => 1,
            'transport_config' => json_encode([
                'driver'       => 'smtp',
                'host'         => 'smtp.mailtrap.io',
                'port'         => '2525',
                'from_name'    => 'Shooting House',
                'from_address' => 'contato@shootinghouse.com.br',
                'encryption'   => 'tls',
                'username'     => 'ce1319547adac4',
                'password'     => 'aec6920f69349c',
            ]),
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'           => 'Shooting House',
            'email'          => 'apis@shootinghouse.com.br',
            'password'       => bcrypt('ramb.='),
            'remember_token' => str_random(10),
        ]);
    }
}

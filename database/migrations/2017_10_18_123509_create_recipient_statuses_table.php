<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipient_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', ['CREATED', 'WAITING', 'FAILED', 'SENDED', 'VIEWED']); // to queue get all != SENDED AND WAITING
            $table->integer('email_recipient_id')->unsigned();
            $table->timestamps();

            $table->foreign('email_recipient_id')->references('id')->on('email_recipients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipient_statuses');
    }
}

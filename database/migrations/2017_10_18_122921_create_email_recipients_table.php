<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_recipients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address');
            $table->text('variables')->nullable();
            $table->enum('type', ['TO', 'CC', 'BCC'])->default('TO');
            $table->boolean('should_queue')->default(1);
            $table->integer('email_id')->unsigned();
            $table->timestamps();

            $table->foreign('email_id')->references('id')->on('emails');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_recipients');
    }
}

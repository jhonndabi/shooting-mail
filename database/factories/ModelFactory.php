<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Email::class, function (Faker\Generator $faker) {
    return [
        'subject'      => $faker->text(250),
        'body'         => $faker->randomHtml(5, 10),
        'from_name'    => $faker->name,
        'from_address' => $faker->unique()->safeEmail,
        'priority'     => $faker->randomElement(['LOWEST', 'LOW', 'MEDIUM', 'HIGH', 'HIGHEST']),
        'attachments'  => factory(App\Models\EmailAttachment::class, $faker->numberBetween(0, 3))->make()->toArray(),
        'recipients'   => factory(App\Models\EmailRecipient::class, $faker->numberBetween(2, 4))->make()->toArray(),
    ];
});

$factory->define(App\Models\EmailRecipient::class, function (Faker\Generator $faker) {
    return [
        'address'      => $faker->unique()->safeEmail,
        'type'         => 'TO',
        'should_queue' => 0,
        'variables' => [
            'name'    => $faker->name,
            'address' => $faker->address,
        ],
    ];
});

$factory->define(App\Models\EmailAttachment::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->imageUrl(),
        'size' => $faker->numberBetween(0, 1000000 * 40),
        'type' => $faker->mimeType,
    ];
});

$factory->define(App\Models\RecipientStatus::class, function (Faker\Generator $faker) {
    return [
        'status' => $faker->randomElement(['waiting', 'failed', 'sended', 'viewed']),
    ];
});

/**
 * Define Factories States
 */

$factory->state(App\Models\Email::class, 'empty_subject', function ($faker) {
    return ['subject' => ''];
});

$factory->state(App\Models\Email::class, 'invalid_subject', function ($faker) {
    return ['subject' => $faker->sentence(256)];
});

$factory->state(App\Models\Email::class, 'invalid_from', function ($faker) {
    return ['from' => $faker->name];
});

$factory->state(App\Models\Email::class, 'empty_recipients_variables', function ($faker) {
    return [
        'recipients' => factory(App\Models\EmailRecipient::class, $faker->numberBetween(1, 1))->states('empty_variables')->make()->toArray(),
    ];
});

$factory->state(App\Models\EmailRecipient::class, 'empty_variables', function ($faker) {
    return ['variables' => null];
});

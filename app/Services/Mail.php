<?php

namespace App\Services;

use App\Jobs\SendEmail;
use App\Models\Client;
use App\Repository\EmailRecipientRepository;
use DB;
use Exception;
use Log;

class Mail
{
    /*
     * Execute the console command.
     *
     * @return mixed
     */
    public static function queue(Client $client)
    {
        try {
            $recipients = EmailRecipientRepository::allShouldQueue($client);

            foreach ($recipients as $recipient) {
                DB::transaction(function () use ($recipient) {
                    SendEmail::dispatch($recipient)->onQueue($recipient->email->priority);
                    $recipient->onQueue();
                });
            }
        } catch (Exception $e) {
            Log::error((string) $e);
        }
    }

    public static function queueAll()
    {
        $clients = Client::orderBy('limit_per_hour')->get();

        foreach ($clients as $client) {
            self::queue($client);
        }
    }
}

<?php

namespace App\Providers;

use Config;
use Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Receive always mail configuration
        // Check on shooting clubs table if domain doesn't exists on clients table
        // $table->string('encryption');

        // if (Schema::hasTable('mails')) {
        //     $mail = DB::table('mails')->first();
        //     if ($mail) { //checking if table is not empty
        //         Config::set('mail.domain', array(
        //             'driver'     => $mail->driver,
        //             'host'       => $mail->host,
        //             'port'       => $mail->port,
        //             'from'       => array('address' => $mail->from_address, 'name' => $mail->from_name),
        //             'encryption' => $mail->encryption,
        //             'username'   => $mail->username,
        //             'password'   => $mail->password,
        //             'sendmail'   => '/usr/sbin/sendmail -bs',
        //             'pretend'    => false,
        //         ));
        //     }
        // }
    }
}

// http://blog.madbob.org/laravel-dynamic-mail-configuration/

// https://laravel.io/forum/07-22-2014-swiftmailer-with-dynamic-mail-configuration

// https://stackoverflow.com/a/45256513
//
// https://github.com/laravel/framework/issues/1849
// https://www.google.com.br/search?q=create+mail+drive+dynamically+laravel&oq=create+mail+drive+dynamically+laravel&gs_l=psy-ab.3...296789.296924.0.301591.2.2.0.0.0.0.159.159.0j1.1.0....0...1.1.64.psy-ab..1.0.0....0.uMr2VkC0r1k
//
// And then registered it in the config\app.php
//
// App\Providers\MailConfigServiceProvider::class,

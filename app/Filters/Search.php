<?php

namespace App\Filters;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Model as BaseModel;

class Search
{
    public static function apply(Request $request, Builder $query)
    {
        return static::applyDecoratorsFromRequest($request->input('filters', []), $query);
    }

    public static function getResults(Request $request, Builder $query)
    {
        return self::apply($request, $query)->get();
    }

    public static function applyFromModel(Request $filters, BaseModel $model)
    {
        return static::apply($filters, $model->newQuery());
    }

    protected static function applyDecoratorsFromRequest(array $filters, Builder $query)
    {
        foreach ($filters as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);
            if (
                static::isValidDecorator($decorator)
                && $query->getModel() instanceof BaseModel
                && in_array($filterName, $query->getModel()->getSearchables())
            ) {
                $query = $decorator::apply($query, $value);
            }
        }

        return $query;
    }

    protected static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\' . studly_case($name);
    }

    protected static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendEmail as SendEmailRequest;
use App\Models\Client;
use App\Models\EmailRecipient;
use App\Models\Transformer\EmailRecipientTransformer;
use App\Repository\EmailRecipientRepository;
use App\Repository\EmailRepository;
use Illuminate\Http\Request;

use App\Repository\Exception\TransactionException;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EmailController extends ApiController
{
    /**
     * Store a email.
     *
     * @param  \App\Http\Requests\SendEmail  $request
     * @param  string  $domain
     * @return \EllipseSynergie\ApiResponse\Contracts\Response
     */
    public function store(SendEmailRequest $request, $domain)
    {
        try {
            $client = Client::where('domain', $domain)->firstOrFail();
            $emails = EmailRepository::create($request->input('email'), $client);
        } catch (ModelNotFoundException $e) {
            return $this->response->errorNotFound('Client Not Found.');
        } catch (TransactionException $e) {
            return $this->response->errorInternalError($e->getMessage());
        } catch (Exception $e) {
            return $this->response->errorInternalError('Something went wrong, try again later.');
        }

        return $this->response->withArray(['success' => "Email(s) stored with success."]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $domain
     * @return \EllipseSynergie\ApiResponse\Contracts\Response
     */
    public function index(Request $request, $domain)
    {
        $recipients = EmailRecipientRepository::paginateSearch($request, $domain, 5);

        return $this->response->withPaginator(
            $recipients,
            new EmailRecipientTransformer
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $domain
     * @param  \App\Models\EmailRecipient  $recipient
     * @return \Illuminate\Http\Response
     */
    public function show($domain, EmailRecipient $recipient)
    {
        return view('mail', [
            'body' => $recipient->body($recipient->email->body)
        ]);
    }
}

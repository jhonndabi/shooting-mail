<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Transformer\ClientTransformer;
use Illuminate\Http\Request;
use App\Http\Requests\Client as ClientRequest;

class ClientController extends ApiController
{
    /**
     * Find a resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \EllipseSynergie\ApiResponse\Contracts\Response
     */
    public function show(Request $request, $domain)
    {
        try {
            $client = Client::where('domain', '=', $domain)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return $this->response->errorNotFound('Client Not Found.');
        } catch (Exception $e) {
            return $this->response->errorInternalError('Something went wrong, try again later.');
        }

        return $this->response->withItem($client, new ClientTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Client  $request
     * @return \EllipseSynergie\ApiResponse\Contracts\Response
     */
    public function store(ClientRequest $request)
    {
        try {
            $client = Client::create($request->input('client'));
        } catch (Exception $e) {
            return $this->response->errorInternalError('Something went wrong, try again later.');
        }

        return $this->response->withArray(['success' => "Client stored with success."]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Client  $request
     * @param  \App\Models\Client  $client
     * @return \EllipseSynergie\ApiResponse\Contracts\Response
     */
    public function update(ClientRequest $request, Client $client)
    {
        try {
            $client->update($request->input('client'));
        } catch (Exception $e) {
            return $this->response->errorInternalError('Something went wrong, try again later.');
        }

        return $this->response->withArray(['success' => "Client updated with success."]);
    }
}

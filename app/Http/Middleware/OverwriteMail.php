<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Mail\TransportManager;

use Closure;
use Mail;
use Config;
use App;

class OverwriteMail
{
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        /*
            $conf is an array containing the mail configuration, a described in
            config/mail.php. Something like

            [
                'driver' => 'smtp',
                'host' => 'smtp.mydomain.com',
                'username' => foo',
                'password' => 'bar'
                ...
            ]
        */
        $conf = my_own_function();

        Config::set('mail', $conf);

        $app = App::getInstance();
        $app->register('Illuminate\Mail\MailServiceProvider');

        return $next($request);
    }
}

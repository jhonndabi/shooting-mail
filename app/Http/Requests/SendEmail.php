<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SendEmail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @see https://laravel.com/docs/5.5/validation#authorizing-form-requests
     * @return bool
     */
    public function authorize()
    {
        // Check here if domain is allowed to send email in all requests include list
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email.subject'      => ['required', 'min:5', 'max:255'],
            'email.body'         => ['required', 'min:5', 'max:65000'],
            'email.priority'     => ['required', Rule::in(['LOWEST', 'LOW', 'MEDIUM', 'HIGH', 'HIGHEST'])],
            'email.from_name'    => ['min:5', 'max:255'],
            'email.from_address' => ['email', 'max:255'],
            'email.recipients.*.address'   => ['required', 'email', 'min:5', 'max:255'],
            'email.recipients.*.variables' => ['array']
        ];
    }
}

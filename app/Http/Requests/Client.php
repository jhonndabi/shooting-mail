<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Client extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @see https://laravel.com/docs/5.5/validation#authorizing-form-requests
     * @return bool
     */
    public function authorize()
    {
        // Check here if domain is allowed to send email in all requests include list
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $client = $this->route('client', null);
        $excludeId = $client ? ",{$client->id},id" : "";

        return [
            'client.domain'         => ['required', 'max:255', "unique:clients,domain{$excludeId}"],
            'client.limit_per_hour' => ['digits_between:1,200'],
            'client.transport_config.driver'       => ['required', Rule::in(['smtp'])],
            'client.transport_config.host'         => ['required', 'max:255'],
            'client.transport_config.port'         => ['required', 'digits_between:1,99999', 'max:255'],
            'client.transport_config.from_name'    => ['required', 'min:5', 'max:255'],
            'client.transport_config.from_address' => ['required', 'email', 'max:255'],
            'client.transport_config.encryption'   => ['required', Rule::in(['tls', 'ssl'])],
            'client.transport_config.username'     => ['required', 'min:5', 'max:255'],
            'client.transport_config.password'     => ['required', 'min:5', 'max:255'],
        ];
    }
}

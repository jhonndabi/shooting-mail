<?php

namespace App\Repository\Exception;

use RuntimeException;

class TransactionException extends RuntimeException
{
    //
}

<?php

namespace App\Repository;

use App\Models\Client;
use App\Models\Email;
use App\Models\Mailer;
use App\Repository\Exception\TransactionException;
use DB;
use Exception;
use Log;

class EmailRepository
{
    public static function create(array $data, Client $client)
    {
        $data['client_id'] = $client->id;
        $email = new Email;
        $email->fill(
            array_only($data, $email->getFillable())
        );

        try {
            // If any error occurs and thrown an exception than rollback will occurs
            DB::transaction(function () use ($email, $data) {
                $email->save();

                $email->recipients()->createMany($data['recipients']);
                $email->attachments()->createMany($data['attachments']);

                self::checkCanSend($email);
            });
        } catch (Exception $e) {
            throw new TransactionException("Can't save email now, try again later.");
        }

        return $email;
    }

    public static function checkCanSend(Email $email)
    {
        if ($email->recipients->count() === 1) {
            try {
                Mailer::send($email->recipients->first());
            } catch (Exception $e) {
                Log::error((string) $e);
            }
        }
    }
}

<?php

namespace App\Repository;

use App\Models\Client;
use App\Models\EmailRecipient;
use DB;
use Illuminate\Http\Request;
use App\Filters\Search;

class EmailRecipientRepository
{
    private static $builder;

    public static function getBuilder()
    {
        if (!self::$builder) {
            self::$builder = (new EmailRecipient)->newQuery();
        }

        return self::$builder;
    }

    public static function allShouldQueue(Client $client)
    {
        $recipients = EmailRecipient::select('email_recipients.*')
                                    ->join('emails', 'emails.id', '=', 'email_recipients.email_id')
                                    ->join('clients', 'clients.id', '=', 'emails.client_id')
                                    ->where('email_recipients.should_queue', '=', '1')
                                    ->where('clients.id', '=', $client->id)
                                    ->whereNotIn('email_recipients.id', function($query) {
                                        $query->select('email_recipient_id')
                                            ->from('recipient_statuses')
                                            ->whereIn('status', ['SENDED', 'VIEWED']);
                                    })
                                    // ->orderByRaw('FIELD(emails.priority, "LOWEST", "LOW", "MEDIUM", "HIGH", "HIGHEST") desc') // if enum is not ordered
                                    ->orderBy('emails.priority', 'desc')
                                    ->orderBy('email_recipients.created_at', 'asc')
                                    ->limit($client->queueLimit())
                                    ->get();

        return $recipients;
    }

    public static function paginateSearch(Request $request, $domain, $perPage = 15)
    {
        $limit = $request->input('limit', $perPage);
        $order = $request->input('order', 'created_at');
        $sort  = $request->input('sort', 'desc');

        $builder = self::getBuilder()->select('email_recipients.*')
                                    ->join('emails', 'emails.id', '=', 'email_recipients.email_id')
                                    ->join('clients', 'clients.id', '=', 'emails.client_id')
                                    ->orderBy($order, $sort);

        return Search::apply($request, $builder)->paginate($limit);
    }
}

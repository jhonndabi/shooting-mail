<?php

namespace App\Models;

class RecipientStatus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status'
    ];

    /**
     * Get the recipient that owns the status.
     */
    public function recipient()
    {
        return $this->belongsTo('App\Models\EmailRecipient');
    }
}

<?php

namespace App\Models\Transformer;

use App\Models\RecipientStatus;

class RecipientStatusTransformer extends AbstractTransformer
{
    public function transform(RecipientStatus $status)
    {
        return [
            'id'         => (int) $status->id,
            'status'     => $status->status,
            'created_at' => $status->created_at, // ->format('d/m/Y H:i:s'),
            'updated_at' => $status->updated_at, // ->format('d/m/Y H:i:s'),
        ];
    }
}

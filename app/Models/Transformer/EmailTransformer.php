<?php

namespace App\Models\Transformer;

use App\Models\Email;

class EmailTransformer extends AbstractTransformer
{
    public function transform(Email $email)
    {
        return [
            'id'         => (int) $email->id,
            'subject'    => $email->subject,
            'created_at' => $email->created_at, // ->format('d/m/Y H:i:s'),
        ];
    }
}

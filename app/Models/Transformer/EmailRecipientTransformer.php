<?php

namespace App\Models\Transformer;

use App\Models\EmailRecipient;
use League\Fractal\ParamBag;

class EmailRecipientTransformer extends AbstractTransformer
{
    /**
     * List of statuses valid Params
     *
     * @var array
     */
    private $statusesValidParams = ['order'];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['email', 'statuses'];

    /**
     * Transform
     *
     * @param \App\Models\EmailRecipient $recipient
     * @return array
     */
    public function transform(EmailRecipient $recipient)
    {
        return [
            'id'         => (int) $recipient->id,
            'address'    => $recipient->address,
            'created_at' => $recipient->created_at,
        ];
    }

    /**
     * Include Email
     *
     * @param \App\Models\EmailRecipient $recipient
     * @return \League\Fractal\Resource\Item
     */
    public function includeEmail(EmailRecipient $recipient)
    {
        $email = $recipient->email;

        return $this->item($email, new EmailTransformer);
    }

    /**
     * Include RecipientStatus
     *
     * @param \App\Models\EmailRecipient $recipient
     * @param \League\Fractal\ParamBag|null
     * @return \League\Fractal\Resource\Collection
     */
    public function includeStatuses(EmailRecipient $recipient, ParamBag $params = null)
    {
        if ($params === null) {
            return $this->collection($recipient->statuses, new RecipientStatusTransformer);
        }

        $this->checkValidParams($params, $this->statusesValidParams);

        list($orderCol, $orderBy) = $params->get('order') ?: ['id', 'asc'];

        $statuses = $recipient->statuses()
            ->orderBy($orderCol, $orderBy)
            ->get();

        return $this->collection($statuses, new RecipientStatusTransformer);
    }
}

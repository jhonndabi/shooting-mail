<?php

namespace App\Models\Transformer;

use App\Models\Client;

class ClientTransformer extends AbstractTransformer
{
    public function transform(Client $client)
    {
        return [
            'id'               => (int) $client->id,
            'domain'           => $client->domain,
            'transport_config' => $client->transport_config,
            'created_at'       => $client->created_at,
            'updated_at'       => $client->updated_at,
        ];
    }
}

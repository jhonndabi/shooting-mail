<?php

namespace App\Models\Enums;

use ReflectionClass;

abstract class AbstractEnum
{
    private static $constCacheArray = [];

    public static function fromValue($id, $default = null)
    {
        $constants = self::inverse();

        if (array_key_exists($id, $constants)) {
            return $constants[$id];
        }

        return $default;
    }

    public static function getConstant($name, $default = null)
    {
        $constants = self::constants();
        $name = mb_strtoupper($name);

        if (array_key_exists($name, $constants)) {
            return $constants[$name];
        }

        return $default;
    }

    public static function inverse()
    {
        return array_flip(self::constants());
    }

    public static function constants()
    {
        $calledClass = get_called_class();

        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }

        return self::$constCacheArray[$calledClass];
    }

    public static function isValidName($name, $strict = false)
    {
        $constants = self::constants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    public static function isValidValue($value, $strict = true)
    {
        $values = array_values(self::constants());
        return in_array($value, $values, $strict);
    }
}

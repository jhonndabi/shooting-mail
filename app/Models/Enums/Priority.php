<?php

namespace App\Models\Enums;

use ReflectionClass;

class Priority extends AbstractEnum
{
    const HIGHEST = 5;
    const HIGH    = 4;
    const MEDIUM  = 3;
    const LOW     = 2;
    const LOWEST  = 1;
}

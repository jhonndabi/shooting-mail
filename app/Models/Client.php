<?php

namespace App\Models;

use Cache;

class Client extends Model
{
    private $queueLimitPercentage = 80;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'domain', 'transport_config', 'limit_per_hour',
    ];

    /**
     * Get the emails for the client.
     */
    public function emails()
    {
        return $this->hasMany('App\Models\Email');
    }

    public function currentSendLimit()
    {
        return $this->attributes['limit_per_hour'] - $this->limitUsed();
    }

    public function queueLimit()
    {
        $queueLimit = (int) floor($this->attributes['limit_per_hour'] * $this->queueLimitPercentage / 100);
        return $queueLimit - $this->limitUsed();
    }

    public function limitUsed()
    {
        return (int) Cache::get($this->limitCacheKey(), 0);
    }

    public function incrementLimitUsed()
    {
        if (Cache::has($this->limitCacheKey()) && $this->limitUsed() > 0) {
            return Cache::increment($this->limitCacheKey());
        }

        return Cache::put($this->limitCacheKey(), 1, 60);
    }

    private function limitCacheKey()
    {
        return "client[{$this->attributes['id']}][emails_limit_used]";
    }

    /**
     * Set the client's trasnport config.
     *
     * @param  array  $value
     * @return void
     */
    public function setTransportConfigAttribute($value)
    {
        $this->attributes['transport_config'] = is_array($value) ? json_encode($value) : $value;
    }

    /**
     * Get the client's trasnport config.
     *
     * @param  string  $value
     * @return array
     */
    public function getTransportConfigAttribute($value)
    {
        return json_decode($value, true);
    }
}

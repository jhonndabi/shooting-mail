<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Schema;

class Model extends EloquentModel
{
    protected $searchable = [];

    /**
     * Get the columns for the model.
     */
    public function columns()
    {
        return Schema::getColumnListing($this->getTable());
    }
}

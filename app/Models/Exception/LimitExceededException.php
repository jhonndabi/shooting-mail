<?php

namespace App\Models\Exception;

use RuntimeException;

class LimitExceededException extends RuntimeException
{
    //
}

<?php

namespace App\Models;

class Email extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_name', 'from_address', 'subject', 'body', 'layout', 'priority', 'client_id'
    ];

    /**
     * Get the attachments for the email.
     */
    public function attachments()
    {
        return $this->hasMany('App\Models\EmailAttachment');
    }

    /**
     * Get the recipients for the email.
     */
    public function recipients()
    {
        return $this->hasMany('App\Models\EmailRecipient');
    }

    /**
     * Get the client that owns the email.
     */
    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function from()
    {
        if (empty($this->from_address) && empty($this->from_name)) {
            return false;
        }

        if (empty($this->from_name)) {
            return [$this->from_address];
        }

        return [$this->from_address, $this->from_name];
    }
}

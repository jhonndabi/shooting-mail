<?php

namespace App\Models;

use App\Models\Exception\LimitExceededException;
use Exception;
use Illuminate\Mail\MailServiceProvider;
use Mail;

class Mailer
{
    public static function updateMailConfig(Client $client)
    {
        $config = $client->transport_config;

        config([
            'mail.driver'       => data_get($config, 'driver', 'smtp'),
            'mail.host'         => $config['host'],
            'mail.port'         => $config['port'],
            'mail.from.name'    => $config['from_name'],
            'mail.from.address' => $config['from_address'],
            'mail.encryption'   => data_get($config, 'encryption', 'tls'),
            'mail.username'     => $config['username'],
            'mail.password'     => $config['password'],
        ]);

        $mailServiceProvider = new MailServiceProvider(app());
        $mailServiceProvider->register();
    }

    public static function send(EmailRecipient $recipient)
    {
        try {
            $client = $recipient->email->client;
            self::updateMailConfig($client);

            if ($client->currentSendLimit() <= 0) {
                throw new LimitExceededException;
            }

            Mail::send(new Mailable($recipient));
            $client->incrementLimitUsed();
        } catch (Exception $e) {
            $recipient->statuses()->create(['status' => 'failed']);
            throw $e;
        }
    }
}

<?php

namespace App\Models;

class EmailRecipient extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address', 'variables', 'email'
    ];

    /**
     * The attributes that are searchable.
     *
     * @var array
     */
    protected $searchable = [
        'email_subject',
    ];

    public function getSearchables()
    {
        return $this->searchable;
    }

    /**
     * Get the email that owns the recipient.
     */
    public function email()
    {
        return $this->belongsTo('App\Models\Email');
    }

    /**
     * Get the statuses for the recipient.
     */
    public function statuses()
    {
        return $this->hasMany('App\Models\RecipientStatus');
    }

    /**
     * Set the recipient's variables.
     *
     * @param  array  $value
     * @return void
     */
    public function setVariablesAttribute($value)
    {
        $this->attributes['variables'] = is_array($value) ? json_encode($value) : $value;
    }

    /**
     * Get the recipient's variables.
     *
     * @param  string  $value
     * @return array
     */
    public function getVariablesAttribute($value)
    {
        return json_decode($value, true);
    }

    public function body()
    {
        $emailBody = $this->email->body;

        if (is_array($this->variables)) {
            foreach ($this->variables as $key => $value) {
                $emailBody = preg_replace("/%recipient.{$key}%/", $value, $emailBody);
            }
        }

        return $emailBody;
    }

    public function onQueue()
    {
        $this->should_queue = 0;
        $this->save();

        $this->statuses()->create(['status' => 'waiting']);
        // $waitingStatus = $this->statuses()->firstOrNew(['status' => 'waiting']);
        // $waitingStatus->touch();
    }
}

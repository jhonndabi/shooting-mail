<?php

namespace App\Models;

use App\Models\EmailRecipient;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable as IlluminateMailable;
use Illuminate\Queue\SerializesModels;

class Mailable extends IlluminateMailable
{
    use Queueable, SerializesModels;

    /**
     * The recipient instance.
     *
     * @var Recipient
     */
    public $recipient;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(EmailRecipient $recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $recipient = $this->recipient;
        $email     = $recipient->email;

        $this->withSwiftMessage(function ($message) use ($recipient) {
            $message->recipient = $recipient;
        });

        if ($email->from()) {
            call_user_func_array([$this, "from"], $email->from());
        }

        return $this->subject($email->subject)
                    ->to($recipient->address)
                    ->view("mail")
                    ->with(['body' => $recipient->body()]);
    }
}

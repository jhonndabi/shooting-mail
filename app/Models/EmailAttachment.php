<?php

namespace App\Models;

class EmailAttachment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'size', 'type'
    ];

    /**
     * Get the email that owns the recipient.
     */
    public function email()
    {
        return $this->belongsTo('App\Models\Email');
    }
}

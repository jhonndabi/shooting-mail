<?php

namespace App\Listeners;

use Illuminate\Mail\Events\MessageSent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSentMessage
{
    /**
     * Handle the event.
     *
     * @param  MessageSent  $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        $recipient = $event->message->recipient;
        $recipient->statuses()->create(['status' => 'sended']);

        if ($recipient->should_queue == 1) {
            $recipient->should_queue = 0;
            $recipient->save();
        }
    }
}

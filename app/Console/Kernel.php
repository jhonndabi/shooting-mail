<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\MailsQueue::class,
        Commands\MailsQueueAll::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        // $schedule->command('mails:queue:all')
        //          ->everyFiveMinute();



    //     $schedule->call(
    //        function(){
    //            // you can pass queue name instead of default
    //            Artisan::call('queue:listen', array('--queue' => 'default'));
    //        }
    //    )->name('ensurequeueisrunning')->withoutOverlapping()->everyMinute();


    // $schedule->command('queue:work')->everyMinute()->withoutOverlapping(); https://gist.github.com/mauris/11375869
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

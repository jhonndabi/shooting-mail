<?php

namespace App\Console\Commands;

use App\Services\Mail;
use Illuminate\Console\Command;

class MailsQueueAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mails:queue:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Queue all clients mails saved on database';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Mail::queueAll();
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Client;
use App\Services\Mail;
use Illuminate\Console\Command;

class MailsQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mails:queue
                        {client : The ID of the client}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Queue mails saved on database by client';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = Client::findOrFail($this->argument('client'));
        Mail::queue($client);
    }
}

<?php

namespace App\Observers;

use App\Models\EmailRecipient;

/**
 * @see https://laravel.com/docs/5.5/eloquent#events
 */
class EmailRecipientObserver
{
    /**
     * Listen to the EmailRecipient created event.
     *
     * @param  EmailRecipient  $recipient
     * @return void
     */
    public function created(EmailRecipient $recipient)
    {
        $recipient->statuses()->create(['status' => 'created']);
    }
}

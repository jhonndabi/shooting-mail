<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {

    Route::group(['prefix' => '{domain}/email'], function () {

        Route::group(['middleware' => ['auth:api']], function () {
            Route::get('/', 'EmailController@index')->name('email-list');
            Route::post('store', 'EmailController@store')->name('email-store');
            Route::get('{recipient}/show', 'EmailController@show')->name('email-show');
        });

        // Route::get('viewed/{recipient}', 'EmailController@viewed')->name('email-viewed');
    });

    Route::group(['prefix' => 'client', 'middleware' => ['auth:api']], function () {
        Route::get('{domain}/show', 'ClientController@show')->name('client-show');
        Route::post('store', 'ClientController@store')->name('client-store');
        Route::post('{client}/update', 'ClientController@update')->name('client-update');
    });
});

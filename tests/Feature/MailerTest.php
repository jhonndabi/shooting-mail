<?php

namespace Tests\Feature;

use App\Models\Client;
use App\Models\Email;
use App\Models\EmailAttachment;
use App\Models\EmailRecipient;
use App\Models\Mailer;
use App\Repository\EmailRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class MailerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    public function fakeEmail()
    {
        $data = factory(Email::class)->make()->toArray();
        $data['client_id'] = Client::firstOrFail()->id;

        return EmailRepository::create($data);
    }
}

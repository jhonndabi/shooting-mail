<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\Models\Email;
use App\Models\Enums\Priority;

class SendEmailTest extends TestCase
{
    private $userToken;

    public function testStoreEmailWithDomainValid()
    {
        $this->callStoreAction('mundotiro.com.br', $this->fakeEmail())
            ->assertStatus(200)
            ->assertJson(['success' => "Email(s) stored with success."]);
    }

    public function testStoreEmailWithDomainInvalid()
    {
        $this->callStoreAction('domain.com.br', $this->fakeEmail())
            ->assertStatus(404)
            ->assertJson(['error' => [
                "code"      => "GEN-NOT-FOUND",
                "http_code" => 404,
                "message"   => "Client Not Found."
            ]]);
    }

    public function testStoreEmailWithSubjectInvalid()
    {
        $email = $this->fakeEmail('invalid_subject');

        $this->callStoreAction('mundotiro.com.br', $email)
            ->assertStatus(422)
            ->assertJson(['email.subject' => ["The email.subject may not be greater than 255 characters."]]);
    }

    public function testStoreEmailWithSubjectEmpty()
    {
        $email = $this->fakeEmail('empty_subject');

        $this->callStoreAction('mundotiro.com.br', $email)
            ->assertStatus(422)
            ->assertJson(['email.subject' => ["The email.subject field is required."]]);
    }

    private function callStoreAction($domain, $email)
    {
        return $this->json(
            'POST',
            "api/v1/{$domain}/email/store",
            ['email' => $email],
            [
                'HTTP_Accept' => 'application/json',
                'HTTP_Authorization' => 'Bearer ' . $this->getToken(),
            ]
        );
    }

    private function getToken()
    {
        $user = User::find('1');

        if (!$this->userToken) {
            $this->userToken = $user->createToken('Shooting Mail Personal Access Client')->accessToken;
        }

        return $this->userToken;
    }

    public function fakeEmail()
    {
        $factory = factory(Email::class);

        if (!empty(func_get_args())) {
            $factory = call_user_func_array([$factory, "states"], func_get_args());
        }

        return $factory->make()->toArray();
    }
}

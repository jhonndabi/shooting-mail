<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Exception;
use Faker\Factory as Faker;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        try {
            $this->faker = Faker::create();

            $app = require __DIR__.'/../bootstrap/app.php';

            $app->make(Kernel::class)->bootstrap();
            app('translator')->setLocale('en');
        } catch (Exception $e) {
            dd(get_class($e), $e->getMessage(), $e->getTraceAsString());
        }

        return $app;
    }
}
